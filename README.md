Create an Openshift Environment and new gitlab project in order to complete small scale automation tasks using selenium. Use a selenium server to complete simple automated tasks with Chrome (open a webpage and click buttons, login, etc.). Use task methods to define simple test cases and gather results. Create a way to access results (text file, PV, database, etc.). Integrate whole process within a gitops pipeline.

### Topics to Research:

    argoCD w/ openshift
    syncing changes b/w gitlab and openshift --permissions issues or due to free version
    --selenium automation-- Done

### Docker Hub images:

    selenium - selenium-chrome/standalone

### Python3 packages to research:

    pytest
    unittest
    selenium

